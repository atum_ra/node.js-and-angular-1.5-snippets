'use strict';

module.exports = function amqpConnectionFactory($amqp, $config, $log, $TimeoutError) {
    const SIZE_1KB = 1024;
    const SIZE_1MB = Math.pow(SIZE_1KB, 2);
    
    let _CONNECTION;
    let _PENDING;

    /**
     * helper for building string
     *
     * @param {String} proto proto
     * @param {String} host hostname
     * @param {Number} port port number
     * @param {String} user auth username
     * @param {String} pass auth password
     * @returns {Promise} connection promise
     */
    function makeConnectionString(proto, host, port, user, pass) {
        return `${proto}://${user}:${pass}@${host}:${port}`;
    }
    
    /**
     * iIt constructs connection
     *
     * @param {String} proto proto
     * @param {String} host hostname
     * @param {Number} port port number
     * @param {String} user auth username
     * @param {String} pass auth password
     * @returns {Promise} connection promise
     */
    function amqpConnectionFactory (proto, host, port, user, pass) {
        $log.info('[SMEVGate::amqpConnection.amqpConnectionFactory] Create new connection for "%s://%s:****@%s:%s"', proto, user, host, port);

        if (!_PENDING) {
            let amqpClient = new $amqp.Client($amqp.Policy.ActiveMQ, {
                receiverLink: {
                    attach: {
                        maxMessageSize: 100 * SIZE_1MB,
                        "max-size-bytes": 100 * SIZE_1MB
                    },
                    "max-size-bytes": 100 * SIZE_1MB
                },
                senderLink: {
                    attach: {
                        maxMessageSize: 100 * SIZE_1MB,
                        "max-size-bytes": 100 * SIZE_1MB
                    },
                    "max-size-bytes": 100 * SIZE_1MB
                },
                parseLinkAddress: function (address) {

                    // hardcode queue prefix as long as we do not use topics
                    const prefix = $config.get('mq.queue_prefix');
                    const result = $amqp.Policy.Default.parseLinkAddress(address);

                    result.name = prefix + result.name;

                    return result;
                }
            });
            
            amqpClient.on('connection:opened', function(ev) {
                $log.silly('[SMEVGate::amqpConnection.amqpConnectionFactory] AMQP connection:opened');
            });
            
            amqpClient.on('connection:closed', function(ev) {
                $log.info('[SMEVGate::amqpConnection.amqpConnectionFactory] AMQP connection:closed', ev);
            });
            
            amqpClient.on('client:errorReceived', function(err) {
                $log.warn('[SMEVGate::amqpConnection.amqpConnectionFactory] AMQP connection:errorReceived', err);
            });
            
            _PENDING = amqpClient.connect(makeConnectionString(proto, host, port, user, pass));
        }
        
        return _PENDING;
    }

    /**
     * @returns {Promise} connection promise
     * @throws {TimeoutError}
     *             timeout error if connection could not be established in given
     *             time
     */
    return function getAmqpConnection () {
        $log.info('[SMEVGate::amqpConnection.getAmqpConnection] Requesting connection');

        const PROTO = $config.get('mq.connection.proto');
        const HOST = $config.get('mq.connection.host');
        const PORT = $config.get('mq.connection.port');

        const USER = $config.get('mq.connection.user');
        const PASS = $config.get('mq.connection.pass');

        // TODO: configuration
        const TIMEOUT = 500;
        let res;

        if (_CONNECTION) {
            $log.silly('[SMEVGate::amqpConnection.getAmqpConnection] Exiting connection present');

            res = Promise.resolve(_CONNECTION);
        } else {
            $log.silly('[SMEVGate::amqpConnection.getAmqpConnection] Creating new connection');

            let timeout = new Promise((resolve, reject) => {
                setTimeout(() => reject(new $TimeoutError('AMQP connection timed out')), TIMEOUT);
            });

            let conn = amqpConnectionFactory(PROTO, HOST, PORT, USER, PASS)
                .then((amqpClient) => {
                    $log.debug('[SMEVGate::amqpConnection.getAmqpConnection] Connection [%s://%s:***@%s:%s] retrieved', PROTO, USER, HOST, PORT);

                    amqpClient._connection.on('connection:connected', function(ev) {
                        $log.silly('[SMEVGate::amqpConnection.getAmqpConnection] AMQP connection:connected', ev);
                    });

                    amqpClient._connection.on('connection:disconnected', function(ev) {
                        $log.silly('[SMEVGate::amqpConnection.getAmqpConnection] AMQP connection:disconnected', ev);
                    });

                    amqpClient._connection.on('connection:errorReceived', function(ev) {
                        $log.debug('[SMEVGate::amqpConnection.getAmqpConnection] AMQP connection:errorReceived', ev);
                    });

                    return amqpClient;
                })
                .catch( (err) => {
                    $log.warn('[SMEVGate::amqpConnection.getAmqpConnection] Could not create new connection', err);

                    return Promise.reject(err);
                });

            res = Promise.race([timeout, conn]).then((conn) => _CONNECTION=conn);
        }

        return res;
    };
};

module.exports['@require'] = [
    'amqp10',
    'config',
    'lib/log',
    'error/timeout-error'
];
module.exports['@singleton'] = true;