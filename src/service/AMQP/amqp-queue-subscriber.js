'use strict';

module.exports = function amqpSubscriberFactory($config, $conn, $log, $NoSuchQueueError) {
    const SUBSCRIBER = {};

    /**
     * It creates template queue
     * @param {String} type of interaction
     * @param {String} name of queue
     * @param {Promise} conn of connection
     * @returns {Promise} sender promise
     */
    function eventLogger (type, name, conn) {
        conn.on('errorReceived', (err) => {
            $log.warn('[SmevGate::amqpQueue.createQueue] %s, for [%s] received error', type, name, err);
        });

        conn.on('attached', (ev) => {
            $log.info('[SmevGate::amqpQueue.createQueue] %s, for [%s] attached', type, name, ev);
        });

        conn.on('detached', (ev) => {
            $log.info('[SmevGate::amqpQueue.createQueue] %s, for [%s] detached', type, name, ev);
        });

        conn.on('creditChange', (ev) => {
            $log.silly('[SmevGate::amqpQueue.createQueue] %s, for [%s] change credit', type, name, ev);
        });

        conn.on('message', (ev) => {
            $log.silly('[SmevGate::amqpQueue.createQueue] %s, for [%s] got message', type, name, JSON.stringify(ev));
        });
        
        return conn;
    }

    /**
     * It creates template queue
     * @param {String} name of queue
     * @returns {Promise} sender promise
     */
    function createQueueReceiver (name) {
        $log.info('[SmevGate::amqpQueue.createQueue] Creating queue receiver [%s]', name);

        let res;
        if (SUBSCRIBER[name]) {
            $log.silly('[SmevGate::amqpQueue.createQueue] Queue receiver [%s] already exists', name);

            res = Promise.resolve(SUBSCRIBER[name]);
        } else {
            $log.silly('[SmevGate::amqpQueue.createQueue] Creating new queue [%s]', name);

            res = $conn().then((amqpClient) => {
                $log.debug('[SmevGate::amqpQueue.createQueue] Creating queue receiver for [%s]', name);

                const receiver = amqpClient.createReceiver(name)
                    .then(eventLogger.bind(null, 'Receiver', name))
                    .then((receiver) => SUBSCRIBER[name] = receiver);

                return receiver;
            });
        }

        return res;
    }

    /**
     * It checks the queue for getting msg
     * @param {String} name of queue
     * @returns {Promise} sender promise
     */
    function getSubscriber (name) {
        const queues = $config.get('mq.queue.receiver');

        $log.info('[SmevGate::amqpQueue.getSubscriber] Retrieving subscriber for queue [%s]]', name);

        if (queues.indexOf(name) > -1) {
            return createQueueReceiver(name);
        }
        $log.warn('[SmevGate::amqpQueue.getSubscriber] No subscriber queue found [%s]  in [%s]', name, queues);
        throw new $NoSuchQueueError(`queue [${name}] not found`);

    }

    return getSubscriber;
};

module.exports['@require'] = [
    'config',
    'factory/amqp-connection',
    'lib/log',
    'error/no-such-queue-error'
];
