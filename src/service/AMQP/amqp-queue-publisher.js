'use strict';

module.exports = function amqpPublisherFactory($config, $conn, $log, $NoSuchQueueError) {
    const PUBLISHER = {};

    /**
     * It creates template queue
     * @param {String} type of interaction
     * @param {String} name of queue
     * @param {Promise} conn of connection
     * @returns {Promise} sender promise
     */
    function eventLogger (type, name, conn) {
        conn.on('errorReceived', (err) => {
            $log.warn('[SmevGate::amqpQueue.createQueue] %s, for [%s] received error', type, name, err);
        });

        conn.on('attached', (ev) => {
            $log.info('[SmevGate::amqpQueue.createQueue] %s, for [%s] attached', type, name, ev);
        });

        conn.on('detached', (ev) => {
            $log.info('[SmevGate::amqpQueue.createQueue] %s, for [%s] detached', type, name, ev);
        });

        conn.on('creditChange', (ev) => {
            $log.silly('[SmevGate::amqpQueue.createQueue] %s, for [%s] change credit', type, name, ev);
        });

        conn.on('message', (ev) => {
            $log.silly('[SmevGate::amqpQueue.createQueue] %s, for [%s] got message', type, name, ev);
        });
        
        return conn;
    }

    /**
     * It creates template queue
     * @param {String} name of queue
     * @returns {Promise} sender promise
     */
    function createQueueSender (name) {
        $log.info('[SmevGate::amqpQueue.createQueue] Creating queue sender [%s]', name);

        let res;

        if (PUBLISHER[name]) {
            $log.silly('[SmevGate::amqpQueue.createQueue] Queue sender [%s] already exists', name);

            res = Promise.resolve(PUBLISHER[name]);
        } else {
            $log.silly('[SmevGate::amqpQueue.createQueue] Creating new queue [%s]', name);

            res = $conn().then((amqpClient) => {
                $log.debug('[SmevGate::amqpQueue.createQueue] Creating queue sender for [%s]', name);

                let sender = amqpClient.createSender(name)
                    .then(eventLogger.bind(null, 'Sender', name))
                    .then((sender) => PUBLISHER[name] = sender);

                return sender;
            });
        }

        return res;
    }

    /**
     * It checks the queue for sending msg
     * @param {String} name of queue
     * @returns {Promise} sender promise
     */
    function getSender (name) {
        let queues = $config.get('mq.queue.sender');

        $log.info('[SmevGate::amqpQueue.getPublisher] Retrieving publisher for queue [%s]', name);

        if (queues.indexOf(name) > -1) {
            return createQueueSender(name);
        }
        $log.warn('[SmevGate::amqpQueue.getPublisher] No publisher queue found [%s] in [%s]', name, queues);
        throw new $NoSuchQueueError(`queue [${name}] not found`);
    }

    return getSender;
};

module.exports['@require'] = [
    'config',
    'factory/amqp-connection',
    'lib/log',
    'error/no-such-queue-error'
];
