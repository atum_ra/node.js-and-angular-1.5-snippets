'use strict';

const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
//const ioc = require('electrolyte');
//const $log = ioc.create('lib/log');

module.exports = function logger($log) {

    function createFolder(folder, cb) {
        folder = path.join(__dirname + '/../../logs/xml/', folder);
        
        fs.stat(folder, (err, stats) => {
            if (err && err.code !== 'ENOENT') return cb(err);
            if (stats && stats.isDirectory()) {
                return cb(null, folder);
            }
            else {
                mkdirp(folder, function (err) {
                    return cb(err, folder);
                });
            }

        });
    }

    return {
        xml: function(data, name) {
            createFolder(name, function (err, folder) {
                if (err) $log.error(err);
                const body = '<?xml version="1.0" encoding="utf-8"?>' + data.toString();

                fs.writeFile(path.join(folder, name + '.xml'), body, function (err) {
                    if (err) $log.error(err);
                });

            });
        },

        answer: function(data, name) {
            createFolder(name, function (err, folder) {
                if (err) $log.error(err);
                const body = '<?xml version="1.0" encoding="utf-8"?>' + data.toString();
                fs.writeFile(path.join(folder, name + '_answer.xml'), body, function (err) {
                    if (err) $log.error(err);
                });
            });
        }
    };

};

module.exports['@require'] = ['lib/log'];

module.exports['@singleton'] = true;