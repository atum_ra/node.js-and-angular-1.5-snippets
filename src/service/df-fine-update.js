'use strict';

module.exports = function dfFineUpdateFactory($log, $got, $config, $lodash, $Promise, $AuthService, $knex) {
	const chunkSize = +$config.get('docs_flow.chunk.size');
	const dfApi = $config.get('docs_flow.api');
	const fineUpdateUrl = `${dfApi}fine/update_by_uin`;
    const authConfig = {
        authorizationSchema: $config.get('authorization.schema'),
        login: $config.get('authorization.systemUser')
    };
    let cachedToken;

    /**
     * It gets token from DB for secure request inside SOA
     * @returns {String} token for request inside service
     */
    function getToken() {
        if (cachedToken && !cachedToken.isRejected) {
            return cachedToken;
        }

        const AuthInstance = $AuthService.login($log, $knex, authConfig);

        cachedToken = AuthInstance.getToken()
            .then(result => result.token)
            .catch(err => $log.error(err));

        return cachedToken;
    }

    /**
     * It updates frontend after request of charge status
     * @param {Object} data with charges
     * @returns {Object} data with charges
     */
	return function dfFineUpdate(data) {

		$Promise.resolve(data)
			.then(data => data.charges.map(charge => {
				return {
					uin: charge.title.UIN,
					status: charge.statusCode === '0' ? 'SENT' : 'ERROR'
				};
			}))
			.then(content => $lodash.chunk(content, chunkSize))
			.mapSeries(chunk => {
				$log.info('Start uploading fines changes chunk to docs_flow...');
				$log.debug('Fines changes chunk content: %j', chunk);

				return getToken()
				.then(token => $got.post(fineUpdateUrl, {
					json: true,
					body: chunk,
					headers: { token }
				}))
				.then(() => {
					$log.info('Finish uploading fines changes chunk.');
				});
			})
			.then(() => {
				$log.info('Finish uploading all fines changes.');
			})
			.catch(err => {
				$log.error(err);
			});

		return data;
	};
};

module.exports['@require'] = [
	'lib/log',
	'got',
	'config',
	'lodash',
	'bluebird',
	'sota-tools-auth',
	'lib/knex-instance'
];
