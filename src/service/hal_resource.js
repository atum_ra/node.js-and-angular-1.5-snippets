'use strict';

/**
 * Returns HAL resource factory.
 *
 * @function halResourceFactory
 * @param  {npm.halberd} $hal hypermedia-aware serialization formatter, which can be represented using JSON and XML
 * @param  {npm.winston.Logger} $log A multi-transport async logging library
 * @return {Function} halResource
 */
function halResourceFactory ($hal, $log) {

    /**
     * Function that returns new HAL resource.
     *
     * @function halResource
     * @param  {Object} [meta] Data object for initialize new HAL resource
     * @param  {String} [url] Url that act as self link to this object
     * @return {Halberd.Resource} halData Instantiated HAL resource
     */
    function halResource (meta, url) {
        const halData = new $hal.Resource(meta, url);

        $log.info('[Psbr::halResourceFactory.halResource] created new [%j]',
            { halResource: halData });

        return halData;
    }

    return halResource;
}

module.exports = halResourceFactory;
module.exports[ '@require' ] = [
    'halberd',
    'lib/log/log'
];
module.exports[ '@singleton' ] = true;
