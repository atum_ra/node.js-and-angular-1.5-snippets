'use strict';

/**
 * Discovery API controller.
 *
 * @function discoveryController
 * @param  {npm.winston.Logger} $log A multi-transport async logging library
 * @param  {Object} $appDescriptor Exports package.json as parsed object
 * @param  {Function} $halResourceFactory HAL instantiated
 * @param  {core.path} $path Node's core path module
 * @return {http.ServerResponse/halberd.Resource} HAL object represents all valid API paths
 */
function discoveryController ($log, $appDescriptor, $halResourceFactory, $path) {
    $log.info('[UNP::discoveryController] instantiated');

    /**
     * Returns HAL object represents all valid API paths.
     *
     * @function getDiscovery
     * @param  {http.ClientRequest} request  Client request
     * @param  {http.ServerResponse} response Server response
     * @param  {Function} next Callback
     */
    function getDiscovery (request, response, next) {
        const halData = $halResourceFactory({}, request.swagger.swaggerObject.basePath);
        const paths = request.swagger.swaggerObject.paths;

        Object.keys(paths).forEach((key) => {
            if ('/' !== key) { // eslint-disable-line yoda
                const path = paths[ key ];
                const link = {
                    href: $path.join(request.swagger.swaggerObject.basePath, key)
                };

                if (/{[^}]+?}/.test(key)) {
                    link.templated = true;
                }

                let name = path[ 'x-discovery-name' ];

                if (!name) {
                    name = key;
                }

                halData.link(name, link);
            }
        });

        // Explicitly add docs
        halData.link('docs', $path.join(request.swagger.swaggerObject.basePath, 'docs?url=/api-docs'));

        // Api version support
        halData.api_version = $appDescriptor.version; // eslint-disable-line camelcase
        response.hal(halData);
        next();
    }

    return getDiscovery;
}

module.exports = discoveryController;
module.exports[ '@require' ] = [
    'lib/log',
    'lib/app-descriptor',
    'service/hal-resource',
    'path'
];
module.exports[ '@singleton' ] = true;
module.exports[ '@async' ] = true;
