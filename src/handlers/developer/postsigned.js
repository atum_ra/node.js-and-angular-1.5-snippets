'use strict';

const config = require('config');

module.exports = function DeveloperListHandlerFactory ($log, $logger, $q, $combining, $requestSOAP) {
    return function DeveloperListHandler (req, res, next) {

        $log.debug('[DeveloperListHandler] postsigned: start');

        const timestamp = ['charges'].concat(new Date().toISOString().split('T'));

        $q.when($combining.getXML(req.body))
            .then((result) => {
                $logger.xml(result.envelope, timestamp);
                return $requestSOAP.transmission(result);
            })
            .then((data) => {
                $logger.answer(data.responseSmev, timestamp);
                return $combining.responseProcess(data);
            })
            .then((data) => {
                res.json(data);
            })
            .catch(next);
      };
};

module.exports['@require'] = [
    'lib/log',
    'service/logger',
    'q',
    config.get('format.version_unp') + 'unp_envelope/combining',
    config.get('format.version_unp') + 'unp_envelope/requestSOAP'
];