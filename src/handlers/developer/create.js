'use strict';

const config = require('config');

module.exports = function ExcelParserHandlerFactory ($log, $q, $parser, $jsonToXml) {
    return function ExcelParserHandler (req, res, next) {
        $log.debug('ExcelParserHandler: start');

        $q.when($parser.parseFile(req.swagger.params.file.value.buffer))
            .then((result) => {
                return $jsonToXml.transformation(result);
            })
            .then((data) => {
                return data;
            })
            .then((data) => {
                // return meaningful result
                res.json(data);
            })
            .catch(next);
            // catch any error and pass next
    };
};

module.exports['@require'] = [
    'lib/log',
    'q',
    config.get('format.version_unp') + 'charge/parsing',
    config.get('format.version_unp') + 'charge/jsonToXml'
];