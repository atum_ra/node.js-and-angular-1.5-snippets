'use strict';

const config = require('config');

module.exports = function DeveloperListHandlerFactory ($log, $logger, $q, $combining, $requestSOAP, $dfFineUpdate) {
    return function DeveloperListHandler (req, res, next) {

        $log.debug('[DeveloperListHandler] postsigned: start');

        const timestamp = ['charges'].concat(new Date().toISOString());

        //TODO: add processing of status only
        $q.when($combining.getPackageStatus(req.body))
            .then($dfFineUpdate)
            .then((data) => {
                    res.json(data);
                })
            .catch(next);
    };
};

module.exports['@require'] = [
    'lib/log',
    'service/logger',
    'q',
    config.get('format.version_unp') + 'UNP_envelope/combining',
    config.get('format.version_unp') + 'UNP_envelope/requestSOAP',
    'service/df-fine-update'
];
