'use strict';

var fs = require('fs');
var xlsx = require('node-xlsx');

/**
 * It gets parsed XLSX, produces XML and validates it
 *
 * @param {Buffer} incomingXLSX XLSX
 * @return {Promise} array
 */
function parseFile (incomingXLSX) {
    return new Promise((resolve, reject) => {
        let parsed = xlsx.parse(incomingXLSX);

        if (parsed.length > 0 && parsed[0].hasOwnProperty('data')) {
            parsed = parsed[0].data;
        } else {
            parsed = reject('empty data');
        }

        return resolve(parsed);
    });
}

module.exports = {
    parseFile
};
