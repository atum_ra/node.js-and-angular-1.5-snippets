'use strict';
var parser = require('xml2json');
var xsd = require('libxml-xsd');
var moment = require('moment');
var uuid = require('node-uuid');
const config = require('config');

const DATE_FORMATS = [
    'DD.MM.YYYY HH:mm:ss',
    'DD.MM.YYYY HH:mm',
    'DD.MM.YYYY',
    moment.ISO_8601
];

/**
 * It gets parsed XLSX, produces XML and validates it
 *
 * @param {Object} incomingDataCreate array reflects XLSX
 * @return {Promise} XML of charges
 */
function transformation (incomingDataCreate) {
     return getXSD().then((schema) => {
        return new Promise((resolve, reject) => {
            incomingDataCreate.splice(0,1);

            var arrayOfCharges = [];
            var promises = [];

            incomingDataCreate.forEach(element => {
                let INNKPP, AltPayerIdentifier;
                if (element[10] && element[10] == 'ИНН+КПП' && element[3] && element[4]) {
                    INNKPP = "2" + element[3] + element[4];
                }
                else if (element[10] && element[10] == 'ИНН' && element[3]) {
                    INNKPP = "4" + element[3];
                }
                else if (element[10] && element[10] == 'Паспорт+Гражданство' && element[27] && element[28]) {
                    AltPayerIdentifier = "01" + normalise_pass_number(element[27]) + element[28];
                }
                const UUID = "a_" + uuid.v4();
                const rub =  element[5];
                const sum = rub * 100; // from roubles to cents

                let date, date_billDate;

                if (isNaN(element[2])) {
                    date = moment(element[2], DATE_FORMATS).toISOString();
                    date_billDate = date.substr(0, 10);
                } else {
                    date = moment.unix((element[2] - 25569) * 86400).toISOString();
                    date_billDate = date.substr(0, 10);
                }

                if (moment(date_billDate).isAfter(moment().add(1, 'days'))) date_billDate = void(0);

                var json = {
                    'chg:Charge': {
                        'xmlns:chg': 'http://roskazna.ru/gisgmp/xsd/116/Charge',
                        'xmlns:com': 'http://roskazna.ru/gisgmp/xsd/116/Common',
                        'xmlns:bdi': 'http://roskazna.ru/gisgmp/xsd/116/BudgetIndex',
                        'xmlns:org': 'http://roskazna.ru/gisgmp/xsd/116/Organization',
                        Id: UUID,
                        supplierBillID: element[0] || '',
                        billDate: date_billDate,
                        //                       ValidUntil: {'$t': moment.unix((element[7] - 25569) * 86400).toISOString()},
                        'chg:SupplierOrgInfo': {
                            'org:Name': {
                                '$t': element[17] || ''
                            },
                            'org:INN': {
                                '$t': element[18] || ''
                            },
                            'org:KPP': {
                                '$t': element[19] || ''
                            },
                            'org:OGRN': {
                                '$t': element[20] || ''
                            },
                            'org:Account': {
                                'org:AccountNumber': {
                                    '$t': element[21] || ''
                                },
                                'org:Bank': {
                                    'org:Name': {'$t': element[22] || ''},
                                    'org:BIK': {'$t': element[24] || ''}
                                }
                            }
                        },
                        'chg:BillFor': {'$t': element[12] || ''},
                        'chg:TotalAmount': {'$t': sum || ''},
                        'com:ChangeStatus': {
                            meaning: element[25] || ''
                            //                Reason: {'$t': '1900-01-01'}
                        },
                        'chg:KBK': {'$t': element[29] || ''},
                        'chg:OKTMO' : {'$t': element[9] || ''},
                        'chg:BudgetIndex': {
                            'bdi:Status': {
//                                '$t': element[11]
                                '$t': '01'
                            },
                            'bdi:Purpose': {
                                xmlns: 'http://roskazna.ru/gisgmp/xsd/116/BudgetIndex',
//                                '$t': element[13]
                                '$t': 'ИД'
                            },
                            'bdi:TaxPeriod': {
                                xmlns: 'http://roskazna.ru/gisgmp/xsd/116/BudgetIndex',
                                '$t': element[14] || ''
                            },
                            'bdi:TaxDocNumber': {
                                xmlns: 'http://roskazna.ru/gisgmp/xsd/116/TaxDocNumber',
                                '$t': element[15] || ''
                            },
                            'bdi:TaxDocDate': {
                                xmlns: 'http://roskazna.ru/gisgmp/xsd/116/BudgetIndex',
                                '$t': element[15] || ''
                            }
                        },
                        'chg:UnifiedPayerIdentifier': {'$t': INNKPP || ''}
                    }

                };

                if (element[25] == '3' && element[26]) json['chg:Charge']['com:ChangeStatus']['com:Reason'] = {'$t' : element[26]};

                if (!INNKPP && AltPayerIdentifier) {
                    json['chg:Charge']['chg:AltPayerIdentifier'] = {'$t' : AltPayerIdentifier};
                    delete json['chg:Charge']['chg:UnifiedPayerIdentifier'];
                }

                if (config.has('unpSrvCode') && !!String(config.get('unpSrvCode'))) {

                    json['chg:Charge']['com:AdditionalData'] = {
                        'com:Name' : {'$t': 'Srv_Code' },
                        'com:Value' : { '$t': config.get('unpSrvCode')}
                    };
                }

                var xml = parser.toXml(json);

                promises.push(new Promise((resol) =>
                    {
                        schema.validate(xml, function(err, validationErr) {
                            let obj = {
                                'data': xml,
                                'title': {
                                    id: UUID,
                                    UIN: json['chg:Charge'].supplierBillID,
                                    date: date,
                                    sum: rub,
                                    payer: INNKPP || AltPayerIdentifier,
                                    status: ['', 'Новое', 'Измененное', 'Аннулированное', 'Деаннулированное'][element[25]]
                                },
                                'error': validationErr ? ['Ошибка валидации документа'] : false,
                                'errorDescription': validationErr ? validationErr.map(error => error.message) : [],
                                'signed': false,
                                'processed': false,
                                'sent': false
                            };

                            arrayOfCharges.push(obj);

                            return resol(obj);
                        });
                    }
                ));

            });

            Promise.all(promises).then(() => {
                return resolve(arrayOfCharges);
            })
                .catch((err) => {

                return reject(err);
            });
        });

    })
         .catch((err) => {
        throw new Error(err);
    });

}

/**
 * It reads XSD schema for charge
 *
 * @return {Promise} XML of charges
 */
function getXSD () {
    return new Promise((resolve, reject) => {
        const originalCwd = process.cwd();
        const tempCwd = originalCwd + '/src/components_UNP_1_16_4/unp_envelope/xsd/';

        console.log('------------', tempCwd);

        try {
            process.chdir(tempCwd);
        } catch (err) {

            return reject(err);
        }

        xsd.parseFile(process.cwd() + '/entity/document/Charge.xsd', (err, schema) => {
            process.chdir(originalCwd);
            if (err) {

                return reject(err);
            }

            return resolve(schema);

        });
    });

}

/**
 * It is helper for validation of pass number
 *
 * @param {String} number of pass
 * @return {String} number
 */
function normalise_pass_number(number) {
    number = String(number).replace(/[/№,-]*/g, '').replace(/\s+/g, '');
    while (number.length<20) {
        number = number + "0";
   }

   return number;
}

module.exports = {
    getXSD,
    transformation,
    normalise_pass_number
};
