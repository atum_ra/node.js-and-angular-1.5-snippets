"use strict";

const xpath = require('xpath');
const xmldom = require('xmldom');
const q = require('q');
const fs = require('fs');
const xsd = require('libxml-xsd');
const ioc = require('electrolyte');
const $log = ioc.create('lib/log');
const $logger = ioc.create('service/logger');
const config = require('config');
const request = require('request');
const $unpCodeService = require('./unpCodeService');
const FIRST_ELEMENT_ONLY_INDEX = 0;

/**
 * After signature of charge in front it packs in soap envelop
 * @param {Object} charges array of singned charges
 * @return {String} XML
 */
function getXML(charges) {
    return q.nfcall(fs.readFile, __dirname + '/resources/requestmsg.xml', 'utf-8')
        .then((templateRequestMsg) => {
            $log.info('requestmsg read');
            return new xmldom.DOMParser().parseFromString(templateRequestMsg);
        })
        .then((rawXmlTemplate) => {

            return charges.reduce((envelope, charge) => {

                if (charge.signed) {
                    let paste = new xmldom.DOMParser().parseFromString(charge.data);

                    paste.removeChild(paste.firstChild);

                    let document = envelope.createElement("pir:Document");
                    document.setAttribute("originatorID",config.get('originatorID'));
                    document.appendChild(paste);

                    xpath.select("//*[local-name() = 'Package' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/PGU_ImportRequest']", envelope)[0].appendChild(document);
                } else {
                    if (!charge.error) {
                        charge.error = [];
                        charge.error.push('This msg was not signed');
                    } else {
                        charge.error.push('This msg was not signed');
                    }

                }

                return envelope;
            }, rawXmlTemplate);
        })
        .then(xml => {

            try {
                return getXSDschema().then((xsdSchema) => {

                    $log.info('xsd read during packaging');
                    return new Promise((resolve, rej) => {
                        try {
                            xsdSchema.validate(xml.toString(), function (err, validationErr) {

                                $log.debug("some problems with validation, if crucial, you'll see details bellow",
                                    {
                                        'error': err,
                                        validationErr
                                    }
                                );

                                    if (err || validationErr) {
                                        charges.forEach((charge) => {
                                            charge.signed = false;
                                            if (!charge.error) {
                                                charge.error = [];
                                                charge.error.push('This msg was not signed within envelope');
                                            } else {
                                                charge.error.push('This msg was not signed within envelope');
                                            }
                                        });
                                    }
                                    if (err) return rej(err);

                                    return resolve(xml);

                            });
                        } catch (e) {
                            $log.debug(e);
                        }

                    });

                });
            } catch (e) {
                $log.debug(e);
            }
        })
        .then(buildEnvelope)
        .then((envelope) => {
            charges.forEach((charge) => {
                charge.sent = true;
                if (charge.error[0] || charge.signed === false) charge.sent = false;

            });

            return {
                envelope,
                charges
            };
        })
        .catch((err) => {
            $log.debug(err);
            throw new Error(err);
        });


}

/**
 * It builds envelope for charges package
 * @param {String|Node} data either serialized xml or DOMNode
 * @returns {String} serialized envelope
 */
function buildEnvelope(data) {
    return q.nfcall(fs.readFile, __dirname + '/resources/envelope.xml', 'utf-8')
        .then((envelope) => {
            envelope = new xmldom.DOMParser().parseFromString(envelope);

            if ('string' == typeof data) {
                data = new xmldom.DOMParser().parseFromString(data);
            }

            let node = xpath.select("//*[local-name() = 'AppData' and namespace-uri() = 'http://smev.gosuslugi.ru/rev120315']", envelope, true);
            node.appendChild(data);

            return new xmldom.XMLSerializer().serializeToString(envelope);
        });
}

/**
 * It reads XSD schema form XML package of charges
 * @returns {Promise}
 */
function getXSDschema() {
    $log.info('getXSD');
    return new Promise((resolve, reject) => {
        $log.debug('DIRNAME',__dirname);
         xsd.parseFile(__dirname + '/xsd/request/Message.xsd', function (err, schema) {
            if (err) $log.debug('xsd err', err);
            if (err) return reject(err);

            return resolve(schema);

        });
    });
}

/**
 * It parses statuses of charge, which system gets asynchronous by separate request on packageID
 * @param {Object} charges which was sent
 * @param {Object} paymentStatusDocument response
 * @return {Object} charges with resultCode
 */
function updateChargesFromXml (charges, paymentStatusDocument) {
    $log.info('updating charges');
    
    const xmlResponseSMEV = new xmldom.DOMParser().parseFromString(paymentStatusDocument.body);
    let packageProcessResult = xpath.select("//*[local-name() = 'PackageProcessResult' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/Ticket']", xmlResponseSMEV, true);
    
    if (packageProcessResult) {
        $log.debug('result received');
        
        let UNP_CODE_LIST = $unpCodeService.getUNPCodeList();

        const processResult = xpath.select("//*[local-name() = 'EntityProcessResult' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/Ticket']", xmlResponseSMEV);

        for (let i = 0; i < processResult.length; i++) {
            let node = processResult[i];
            let id = node.getAttribute('entityId');
            
            let charge = charges.filter(charge => charge.title.id === id).pop();
            
            let resultCodeSMEV = xpath.select("//*[local-name() = 'ResultCode' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/ErrInfo']", node, true).textContent;
            let resultDataSMEV = xpath.select("//*[local-name() = 'ResultData' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/ErrInfo']", node, true);
            resultDataSMEV = resultDataSMEV && resultDataSMEV.textContent;
            let resultDescriptionSMEV = xpath.select("//*[local-name() = 'ResultDescription' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/ErrInfo']", node, true);
            resultDescriptionSMEV = resultDescriptionSMEV && resultDescriptionSMEV.textContent;

            let descr = [];
            if (resultDataSMEV) {
                descr.push(resultDataSMEV);
            }
            if (resultDescriptionSMEV) {
                descr.push(resultDescriptionSMEV);
            }
            descr = descr.join(': ');
            
            charge.resultDescription = descr;
            charge.statusCode = resultCodeSMEV;
        }
        
        return {
            statusCode: 0,
            charges
        };

    } else {
        $log.debug('progress received');
        
        let resultCodeSMEV = xpath.select("//*[local-name() = 'ResultCode' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/ErrInfo']", xmlResponseSMEV, true).textContent;
        let resultDataSMEV = xpath.select("//*[local-name() = 'ResultData' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/ErrInfo']", xmlResponseSMEV, true);
        resultDataSMEV = resultDataSMEV && resultDataSMEV.textContent;
        let resultDescriptionSMEV = xpath.select("//*[local-name() = 'ResultDescription' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/ErrInfo']", xmlResponseSMEV, true);
        resultDescriptionSMEV = resultDescriptionSMEV && resultDescriptionSMEV.textContent;

        let descr = [];
        if (resultDataSMEV) {
            descr.push(resultDataSMEV);
        }
        if (resultDescriptionSMEV) {
            descr.push(resultDescriptionSMEV);
        }
        descr = descr.join(': ');
        
        return {
            statusCode: resultCodeSMEV,
            resultDescription: descr,
            charges: charges.map(charge => {
                charge.statusCode = resultCodeSMEV;
                charge.resultDescription = charge.error && charge.error.length && 'Импорт не проводился' || descr;
                
                return charge;
            })
        };
    }
}

/**
 * It starts request for resultCode for charges
 * @param {Object} charges which was sent
 * @param {Object} paymentStatusDocument response
 * @return {Object} charges with resultCode
 */
function getPackageStatus (body) {
    return getPaymentStatusDocument(body.packageId)
        .then(updateChargesFromXml.bind(null, body.charges))
        .then(data => {
            data.packageId = body.packageId;
            
            return data;
        });
}

/**
 * It processes the response of transmission and returns codes for each charge
 * @param data is result code of request
 * @returns {Object}
 */
function responseProcess (data) {
    if (data.responseSmev) {

        let xmlResponseSMEV = new xmldom.DOMParser().parseFromString(data.responseSmev);
        
        let resultCodeSMEV = xpath.select("//*[local-name() = 'ResultCode' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/ErrInfo']", xmlResponseSMEV, true).textContent;
        // package id

        let resultDataSMEV = xpath.select("//*[local-name() = 'ResultData' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/ErrInfo']", xmlResponseSMEV, true);
        resultDataSMEV = resultDataSMEV ? resultDataSMEV.textContent : 'не получено';
        let resultDescriptionSMEV = $unpCodeService.getDescriptionByUNPCode(resultCodeSMEV);
        let UNP_CODE_LIST = $unpCodeService.getUNPCodeList();
        
        $log.info('result: %s', resultCodeSMEV, resultDescriptionSMEV);

        switch (resultCodeSMEV) {
            case UNP_CODE_LIST.PROCESSING.CODE:
                $log.info("[$combining.responseProcess] resultCodeSMEV = [%s], description : [%s]", resultCodeSMEV, resultDescriptionSMEV);

                return {
                    charges: data.charges.map(charge => Object.assign(charge, {
                        resultDescription: charge.error && charge.error.length && 'Импорт не проводился' || 'Проверка статуса начисления'
                    })),
                    packageId: resultDataSMEV,
                    statusCode: 50
                };

            default:
                $log.error("[$combining.responseProcess] error, resultCodeSMEV = [%s], description : [%s]", resultCodeSMEV, resultDescriptionSMEV);

                return Promise.reject({
                    message: '[$combining.responseProcess] error, resultCodeSMEV = ' + resultDescriptionSMEV,
                    code: resultCodeSMEV
                });
        }

    } else {
        $log.error('empty response');
    }
}

/**
 * Takes package id and template and combines an xml from them
 * @param {String} packageId it of package to ask status of
 * @param {String} template xml template to use for status request
 * @returns {String} XML
 */
function buildStatusRequestXml(packageId, template) {
    let statusRequestXml = new xmldom.DOMParser().parseFromString(template);

    let node = xpath.select("//*[local-name() = 'PackageID' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/PackageStatusRequest']", statusRequestXml, true);
    node.appendChild(statusRequestXml.createTextNode(packageId));

    return new xmldom.XMLSerializer().serializeToString(statusRequestXml);
}

/**
 * It processes envelope building and request for resultCodes of Charges
 * @param {String} paymentId Id of package
 * @returns {Promise} Promise
 */
function getPaymentStatusDocument(paymentId) {

    const timestamp = ['statuses'].concat(new Date().toISOString().split('T'));
    
    return q.nfcall(fs.readFile, __dirname + '/resources/status-request.xml', 'utf-8')
        .then(template => {
            buildStatusRequestXml.bind(null, paymentId, template);
        })
        .then(data => {

            $logger.xml(data, timestamp);

            return {
                url: 'http://' + config.get('unpRequest.host') + ":" + config.get('unpRequest.port') + config.get('unpRequest.path'),
                headers: {
                    "Content-Type": "text/xml;charset=UTF-8",
                    "SOAPAction": "http://roskazna.ru/gisgmp/02000000/SmevGISGMPService/GISGMPTransferMsg"
                },
                body: data
            };
        }).then(params => new Promise((resolve, reject) => {
            $log.info('Requesting status');

            requestStatus(params, resolve, reject);
        }))
        .then((data) => {
            $logger.answer(data.body, timestamp);
            return data;
        })
        .catch((err) => {
            $log.error(err);
            return err;
        });
}

/**
 * It helps to make http request
 * @param {Object} params from request
 * @param {Object} resolve
 * @param {Object} reject
 * @returns {Object} Promise resolve\reject
 */
function requestStatus (params, resolve, reject) {
    request.post(params, (error, response, body) => {
        $log.info('Status response received');
        if(error || response.statusCode != 200){
            $log.error('It returns error requestStatus', error || response.statusCode);
            return reject({message:'Возникла ошибка при запросе статуса начислений',
            code: response.statusCode });
        }

        let xmlResponseSMEV = new xmldom.DOMParser().parseFromString(body);

        let requestProcessResult = xpath.select("//*[local-name() = 'RequestProcessResult' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/Ticket']", xmlResponseSMEV, true);
        if (requestProcessResult) {

            let resultCodeSMEV = xpath.select("//*[local-name() = 'ResultCode' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/ErrInfo']", xmlResponseSMEV, true).textContent;
            let resultDescriptionSMEV = xpath.select("//*[local-name() = 'ResultDescription' and namespace-uri() = 'http://roskazna.ru/gisgmp/xsd/116/ErrInfo']", xmlResponseSMEV, true).textContent;

            $log.info(resultDescriptionSMEV, resultCodeSMEV);

            return resolve(response);
        }

        return resolve(response);

    });
}

module.exports = {

    getXML,
    getXSDschema,
    buildEnvelope,
    responseProcess,
    getPackageStatus

};
