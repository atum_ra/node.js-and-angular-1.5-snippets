/**
 * Created by arefevra on 20.06.16.
 */
"use strict";
var http = require('http');
let config = require('config');
const ioc = require('electrolyte');
const $log = ioc.create('lib/log');
const _ = require('lodash');

/**
 * It requests outside SOAP service
 * @param {Object} dataXml with charges
 * @returns {Promise} Promise resolve\reject
 */
function transmission(dataXml) {

    return new Promise((resolve, rej) => {
        const body = '<?xml version="1.0" encoding="utf-8"?>' + dataXml.envelope.toString();

        let postRequest = _.cloneDeep(config.unpRequest);

        postRequest.timeout = +postRequest.timeout;
        postRequest.headers['Content-Length'] = Buffer.byteLength(body);

        let buffer = "";

        $log.info('unp request sent to', postRequest);
        const req = http.request( postRequest, function( res )    {
            if (res.statusCode !== 200) {
                console.log('UNP::requestSOAP.request UNP statusCode', res.statusCode);

                return rej({
                    message: 'UNP::requestSOAP.request UNP statusCode ' + res.statusCode,
                    code: 404});
            }
            res.on( "data", function( data ) {
                buffer = buffer + data;
            });


            res.on( "end", function() {
                dataXml.responseSmev = buffer;

                return resolve ( dataXml );
            });

        });

        req.on('error', function(e) {
            $log.info(`problem with request: ${e.message}`);

            return rej(e);
        });

        req.write( body );
        req.end();
    });
}

module.exports = {
    transmission
};


