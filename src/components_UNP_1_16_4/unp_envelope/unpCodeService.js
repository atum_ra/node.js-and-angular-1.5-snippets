/**
 * Created by arefevra on 25.11.16.
 */
'use strict';

function getUNPCodeList() {
    return {
        /**
         * constant for UNP response codes
         * @type {{PROCESSING: {CODE: string, DESCRIPTION: string},
         * XML_ERROR: {CODE: string, DESCRIPTION: string},
         * EP_OV_ERROR: {CODE: string, DESCRIPTION: string},
         * CHECK_NUMBER_OF_ENTITIES_IN_PACKAGE_ERROR: {CODE: string, DESCRIPTION: string}}}
         */

            PROCESSING: {
                CODE: "0",
                DESCRIPTION: "Пакет принят в обработку"
            },
            XML_ERROR: {
                CODE: "11",
                DESCRIPTION: "Установлено несоответствие XML-схеме"
            },
            EP_OV_ERROR: {
                CODE: "27",
                DESCRIPTION: "Установлена ошибка в ЭП-ОВ"
            },
            PROCESSING_INCOMPLETE: {
                CODE: "50",
                DESCRIPTION: "Обработка пакета не окончена"
            },
            CHECK_NUMBER_OF_ENTITIES_IN_PACKAGE_ERROR: {
                CODE: "R44",
                DESCRIPTION: "Проверка количества сущностей в пакете"
            }
    };
}

function getDescriptionByUNPCode(code) {
    const UNP_CODE_LIST = getUNPCodeList();
    let description = '';

    Object.keys(UNP_CODE_LIST).forEach((key) => {
        if(UNP_CODE_LIST[key].CODE == code){
            description =  UNP_CODE_LIST[key].DESCRIPTION;
        }
    });

    return description;
}

module.exports = {
    getUNPCodeList,
    getDescriptionByUNPCode
};

