'use strict';

module.exports = function errorMiddlewareFactory ($log, $code) {

    /**
     * Simple connect/express error middleware
     *
     * @param {Object} err request
     * @param {Object} req request
     * @param {Object} res response
     * @param {Function} next next handler in chain
     * @return {Object} original response
     */
    /* eslint no-unused-vars: 0 */
    return function errorMiddleware (error, request, response, next) {
        $log.error(error);

        let message = error.message || 'Unknown error';
        let code = error.code || $code.ERROR;
        let errors = [];

        if (error.code === 'SCHEMA_VALIDATION_FAILED') {
            code = $code.ERROR_VALIDATION;
            message = error.message;
            errors = error.results.errors;
        }

        if (error.code === 'ETIMEDOUT') {
            code = 404;
            message = 'Not connected via VPN';
            errors = null;
        }

        response
            .status(code)
            .json({
                code,
                message,
                errors
            });
    };
};

module.exports['@require'] = ['lib/log', 'lib/code-response.js'];