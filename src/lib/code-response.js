/**
 * Created by Oleg Rusak on 03/06/16.
 */

'use strict';

exports = module.exports = function CodeResponseFactory() {
    return {
        OK: 200,
        NOT_MODIFIED_CODE: 304,
        ERROR_VALIDATION: 406,
        ERROR: 500
    }
};

exports['@singleton'] = true;
