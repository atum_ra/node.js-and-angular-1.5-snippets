'use strict';

function LoggerFactory ($log4js) {

    //log4js.addAppender(fluent.support.log4jsAppender('default', {
    //  host: 'localhost',
    //  port: 24224,
    //  timeout: 3.0
    //}));

    const log = $log4js.getLogger('default');

    log.info('Node instance start up');

    // It supports the code with log.silly of sota-tools-auth
    log.silly = function(...args) {
        log.trace(...args);
    };

    return log;
}
module.exports = LoggerFactory;
module.exports['@require'] = [
    'log4js'
];
module.exports['@singleton'] = true;
