'use strict';

module.exports = function KnexInstanceFactory ($config, $knex ) {
	if (!$knex) {
		throw new Error('knex-instance. Not found knex lib');
	}
	if (!$config.get('db.connection')) {
		throw new Error('knex-instance. Not found option db.connection');
	}

	return new $knex({
		client: 'postgresql',
		connection: $config.get('db.connection')
	});
};

module.exports['@require'] = [
	'config',
	'knex'
];

module.exports['@singleton'] = true;
