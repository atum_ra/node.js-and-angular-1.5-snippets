'use strict';

const log4js = require('log4js');
const fluent = require('fluent-logger');

//log4js.addAppender(fluent.support.log4jsAppender('audit', {
//   host: 'localhost',
//   port: 24224,
//   timeout: 3.0
//}));

module.exports = function AuditLoggerFactory () {
    const logger = log4js.getLogger('audit');
    
    return logger;
}
module.exports['@singleton'] = true;