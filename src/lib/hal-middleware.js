'use strict';

module.exports = function halMiddlewareFactory () { 

    /**
     * Simple connect/express middleware for halbert
     *
     * @param {Object} req request
     * @param {Object} res response
     * @param {Function} next next handler in chain
     * @return {Object} original response
     */
    return function halMiddleware (req, res, next) {
        const DEFAULT_STATUS = 200;
        const DEFAULT_CONTENT_TYPE = 'application/hal+json';

        /* eslint no-param-reassign: 0 */
        res.hal = (resource) => {
            res.status(DEFAULT_STATUS)
               .type(DEFAULT_CONTENT_TYPE)
               .json(resource.toJSON());
        };
        return next();
    };
};
