'use strict';
const fs = require('fs');
const jsonToXml = require ('../../../src/components_UNP_1_16_4/charge/jsonToXml');
const parsing = require ('../../../src/components_UNP_1_16_4/charge/parsing');

describe('jsonToXml', () => {

    it('read xslx and produce xml ',() => {

        let flag = false;
        let _res;
        const file = fs.readFileSync('test/new_16_08.xlsx');

        runs(() => {
            parsing.parseFile(file)
                .then(data => jsonToXml.transformation(data))
                .then((res) => {
                    flag = true;
                    _res=res;

                    return true;
                });
        });

        waitsFor(() => {
            return flag;
        }, 'waits for response', 5000);

        let result= `<chg:Charge xmlns:chg="http://roskazna.ru/gisgmp/xsd/116/Charge" xmlns:com="http://roskazna.ru/gisgmp/xsd/116/Common"
        xmlns:bdi="http://roskazna.ru/gisgmp/xsd/116/BudgetIndex" xmlns:org="http://roskazna.ru/gisgmp/xsd/116/Organization"
        supplierBillID="0318607100000000000150214" billDate="2017-07-24"><chg:SupplierOrgInfo>
        <org:Name>Управление Федерального казначейства по Московской области (Главгосстройнадзор Московской области)</org:Name>
        <org:INN>7707029720</org:INN><org:KPP>507501001</org:KPP><org:OGRN>5077746887312</org:OGRN>
        <org:Account><org:AccountNumber>40101810845250010102</org:AccountNumber><org:Bank><org:Name>ОТДЕЛЕНИЕ 1 Москва</org:Name>
        <org:BIK>044583001</org:BIK></org:Bank></org:Account></chg:SupplierOrgInfo><chg:BillFor>уплата штрафа</chg:BillFor>
        <chg:TotalAmount>1005400004000</chg:TotalAmount><com:ChangeStatus meaning="1"></com:ChangeStatus>
        <chg:KBK>02611690050050000140</chg:KBK><chg:OKTMO>46649000</chg:OKTMO><chg:BudgetIndex><bdi:Status>01</bdi:Status>
        <bdi:Purpose xmlns="http://roskazna.ru/gisgmp/xsd/116/BudgetIndex">ИД</bdi:Purpose>
        <bdi:TaxPeriod xmlns="http://roskazna.ru/gisgmp/xsd/116/BudgetIndex">0</bdi:TaxPeriod>
        <bdi:TaxDocNumber xmlns="http://roskazna.ru/gisgmp/xsd/116/TaxDocNumber">0</bdi:TaxDocNumber>
        <bdi:TaxDocDate xmlns="http://roskazna.ru/gisgmp/xsd/116/BudgetIndex">0</bdi:TaxDocDate></chg:BudgetIndex>
        <chg:AltPayerIdentifier>0150105536120000000000643</chg:AltPayerIdentifier></chg:Charge>`;

        runs(() => {
            expect(_res[_res.length-1].data.replace(/Id="a_\S+\s/, '')).toEqual(result);
        });

    });

     it('read and convert pass number 2222№555555',() => {
         let a= jsonToXml.normalise_pass_number('2222№555555');
         expect(a).toEqual('22225555550000000000');
     });

     it('read and convert pass number 2222 555555',() => {
         let a= jsonToXml.normalise_pass_number('2222 555555');
         expect(a).toEqual('22225555550000000000');
     });
});
