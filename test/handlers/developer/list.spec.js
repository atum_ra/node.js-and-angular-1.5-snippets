'use strict';

const IoC = require('electrolyte');

xdescribe('DeveloperListHandler', () => {
    let container,
        $log;

    beforeEach(() => {
        $log = {
          debug: jasmine.createSpy()
        };

        container = new IoC.Container();
        container.use(IoC.node_modules());
        container.use(IoC.dir('src'));
        container.use((id) => {
          let module;
          
          if (id === 'lib/log') {
            $log['@literal'] = true;
            
            module = $log;
          }
          
          return module;
        });
    });

    it('Should log handler invocation', () => {
        const handler = container.create('handlers/developer/list');

        const req = {
            swagger: {
                swaggerObject: {}
            }
        };
        
        const res = {
          status: () => res,
          type: () => res,
          json: () => res
        };

        handler(req ,res);

        expect($log.debug).toHaveBeenCalled();
    });
});
