'use strict';
const fs= require('fs');
const IoC = require('electrolyte');

describe('service/logger', () => {
    let container,
        $log;

    beforeEach(() => {
        $log = {
            debug: jasmine.createSpy('spy')
        };

        container = new IoC.Container();
        container.use(IoC.node_modules());
        container.use(IoC.dir('src'));

    });

    afterEach(() => {
        fs.unlink('logs/xml/request_test/request_test.xml', (err) => {
            if (err) $log.debug(err);
            fs.rmdir('logs/xml/request_test', (err) =>  {if (err) $log.debug(err);});

        });
    });

    it('Should save xml request', () => {
        const logger = container.create('service/logger');

        const data= '<chg:Charge xmlns:chg="http://roskazna.ru/gisgmp/xsd/116/Charge">';
        const name= 'request_test';

        let flag=false;

        runs(() => {
            logger.xml(data, name);
            setTimeout(() => {
                flag = true;
            }, 4000);
        });

        waitsFor(() => {

            return flag;
        },
        'waits for response', 6000);

        runs(() => {
            const file= fs.readFileSync('logs/xml/request_test/request_test.xml', 'utf8');
            expect(file.replace('<?xml version="1.0" encoding="utf-8"?>','')).toEqual(data);
        });
    });

});

