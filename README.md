# Service UNP

UNP service is one of integration services in SOA system, which supports business process of large agency.
It communicates with outside finance system via unified SOAP protocol, with inside services through Message broker.

##  Communication diagram (high scale view)

 This is high scale view of service UNP among SOA application.
 
 ![This](./unp0_flow.png)

## Communication diagram of UNP service (module level)

This is a communication diagram between modules in UNP service.

![This](./unp_flow.png)

Modules interact with outside of developed SOA system secured web-services.
Web-interface docsFlow is the module of separate service which provides UI for interaction, other modules are a part of UNP service.

## Technologies:

* XML, XML DOM manipulation and XSD validation
* Swagger schema and routing
* Implementation of Inversion of Control approach (left in examples for handlers only)
* Excel parsing
* Promising with Bluebird
* AMQP usage
* Streams with http requests

**N.B. At repository there is a part of service. This essential part is shown at diagram. The service has been started 2 years ago that is why something is looked old-fashioned, most tests were removed**