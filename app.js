'use strict';

const express = require('express');
const http = require('http');
const swaggerTools = require('swagger-tools');
var bodyParser = require('body-parser');
const cors = require('cors');

const config = require('config');

// Instantiate app
const app = express();

// register IoC
const ioc = require('electrolyte');

const multer = require('multer');
const storage = multer.memoryStorage();
const upload = multer({
  storage: storage,
  limit: Infinity
});


// initialize DI
ioc.use(ioc.node_modules());
ioc.use(ioc.dir('src'));
ioc.use('handlers', ioc.dir('src/handlers'));


// swaggerRouter configuration
const options = {
  controllers: './api-routes',

  /* eslint no-process-env: 0 */
  useStubs: process.env.NODE_ENV === 'test' // Conditionally turn on stubs (mock mode)
};

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
const swaggerDoc = require('./swagger/api-doc.js');


app.use(bodyParser.json({limit: '10mb'}));

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, (middleware) => {

    app.use(cors({
        allowCredentials: true,
        allowHeaders: 'Content-Type, accept, token',
        allowMethods: 'GET, POST, OPTIONS, PUT, DELETE',
        allowOrigins: '',
        denyOrigins: '',
        exposeHeaders: 'x-request-time',
        maxAge: 133734,
        requireOrigin: true,
        origin: 'http://localhost:9001',
        credentials: true
    }));

    // Setup HAL middleware
    app.use(ioc.create('lib/hal-middleware'));

    // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
    app.use(middleware.swaggerMetadata());

    // Validate Swagger requests
    app.use(middleware.swaggerValidator({
      validateResponse: false
    }));

    // Route validated requests to appropriate controller
    app.use(middleware.swaggerRouter(options));

    // Serve the Swagger documents and Swagger UI
    app.use(middleware.swaggerUi());

    // Setup error handler
    app.use(ioc.create('lib/error-middleware'));

    /* eslint no-process-env: 0 */
    const serverHost = config.get('server.host');
    const serverPort = config.get('server.port');
    // Start the server
    http.createServer(app).listen(serverPort, serverHost, () => {

        /* eslint no-console: 0 */
        console.log('Your server is listening on http://%s:%d', serverHost, serverPort);
    });
});

