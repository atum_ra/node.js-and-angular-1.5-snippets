'use strict';

const ioc = require('electrolyte');
const multer = require('multer');

const storage = multer.memoryStorage();
const upload = multer({
    storage,
    limit: Infinity
});

module.exports = {
    post: ioc.create('handlers/developer/postsigned')
};