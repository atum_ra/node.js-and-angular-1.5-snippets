'use strict';

const ioc = require('electrolyte');

module.exports = {
  post: ioc.create('handlers/developer/create')
};
