'use strict';

// args.apiDoc needs to be a js object.  This file could be a json file, but we can't add
// comments in json files.
module.exports = {
  swagger: '2.0',

  // all routes will now have /v1 prefixed.
  basePath: '/unp/v1',

  produces: [
    'application/hal+json',
    'application/json'
  ],

  info: {
    title: 'UNP API',
    version: '1.0.0'
  },

  definitions: {
    Error: {
      additionalProperties: true
    },
    Developer: {
      $ref: './swagger/schemas/developer.jsd'
    },
    DeveloperResult: {
      $ref: './swagger/schemas/developer-hal.jsd'
    },
    DevelopersResult: {
      $ref: './swagger/schemas/developers-hal.jsd'
    }
  },

  // paths are derived from args.routes.  These are filled in by fs-routes.
  paths: {
    '/developer': {
      parameters: [],
      get: {
        description: 'Returns list of all developers without',
        operationId: 'get',
        parameters: [
          {
            in: 'query',
            name: 'q',
            type: 'string'
          }
        ],
        responses: {
          200: {
            description: 'Unsorted list of developers',
            schema: {
              $ref: '#/definitions/DevelopersResult'
            }
          },
          204: {
            description: 'Applied query filter returns no results'
          }
        }
      },
      post: {
        'x-swagger-router-controller': 'developer',
        description: 'Returns xlsx',
        operationId: 'post',
        parameters: [
          {
            in: 'formData',
            name: 'file',
            type: 'file'
          }
        ],
        responses: {
          200: {
            description: 'Resulting doc',
            schema: {
//              $ref: '#/definitions/DevelopersResult'
            }
          }
        }
      }
    },
    '/postsigned': {
        post: {
        'x-swagger-router-controller': 'postsigned',
        description: 'Returns signed',
        operationId: 'post',
        parameters: [
          {
            in: 'body',
            name: '0',
            "schema": {
              "type": "string"
            }

          }
        ],
        responses: {
          200: {
            description: 'Resulting doc',
            schema: {
//              $ref: '#/definitions/DevelopersResult'
            }
          }
        }
      }
    },
    '/charge-status': {
        post: {
        'x-swagger-router-controller': 'status',
        description: 'Checks charge status',
        operationId: 'post',
        parameters: [
          {
            in: 'body',
            name: '0',
            "schema": {
              "type": "string"
            }

          }
        ],
        responses: {
          200: {
            description: 'Resulting doc',
            schema: {
//              $ref: '#/definitions/DevelopersResult'
            }
          }
        }
      }
    },
    '/quittance': {
        parameters: [],
        post: {
          'x-swagger-router-controller': 'quittance',
          description: 'Accepts xlsx file and asks UNP for change quittances',
          operationId: 'post',
          parameters: [
            {
              in: 'formData',
              name: 'file',
              type: 'file'
            }
          ],
          responses: {
            200: {
              description: 'Resulting doc',
              schema: {
//                $ref: '#/definitions/DevelopersResult'
              }
            }
          }
        }
      }
    
  }
};
